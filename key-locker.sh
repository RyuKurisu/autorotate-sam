#!/bin/bash
# Create a rotation lock file (/dev/shm/.rotation-lock) and create a inotifywait while loop to enable/disable the keyboard

KEYBOARD=$(xinput list | grep -e 'AT.*keyboard' -e 'Synaptics' | grep -Po 'id=\K[0-9]+')

function Keyboard {
	[[ $1 == "1" ]] && action="enable"
	[[ $1 == "0" ]] && action="disable"
	while read i; do
		xinput $action $i;
	done <<< "$KEYBOARD";
}

if [ -f /dev/shm/.rotation-lock ] ; then
	rm /dev/shm/.rotation-lock # Remove the rotation lock file
	kdialog --passivepopup "Rotation allowed." 2 --icon=rotation-allowed-symbolic
	yad --notification --command=/home/ryukurisu/.bin/keyboard-lock.sh --image='kxkb' & # Provide a system tray icon to disable or enable the keyboard
	while inotifywait -e create -e delete /dev/shm/; do
		if [ ! -f /dev/shm/.keyboard-lock ]; then
			Keyboard 1;
		else
			Keyboard 0;
		fi
	done
else
	> /dev/shm/.rotation-lock # Add the rotation lock file
	kdialog --passivepopup "Rotation locked" 2 --icon=rotation-locked-symbolic
	killall yad
fi
