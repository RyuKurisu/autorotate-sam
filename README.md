# Autorotate Screen And Mice

This script monitors a log-file in a tmpfs [RAM] folder with inotifywait for modification (aka orientation change).  All **pointers** and the correct **display** should be reorientated with `xrandr` & `xinput "Coordinate Transformation Matrix"`.

This is a heavily modified script originally found on GitHub.com: https://github.com/gevasiliou/PythonTests/blob/master/autorotate.sh

**HOW IT WORKS**

Step 1] Add autorotate-sam.sh and to your start up programs.  "S.A.M." is for rotating screen and mice (aka pointers).

Step 2] Add key-locker.sh to a keyboard shortcut.  It'll add/remove a hidden file [`.rotation-lock`] to tmpfs which nullifies the autorotate script.  When rotation is enabled it also creates a systray icon that adds/removes another hidden file [`.keyboard-lock`] to (un)locks the keyboard.
