# Autorotate Screen And Mice

This project is heavily inspired by a script originally found on GitHub.com: https://github.com/gevasiliou/PythonTests/blob/master/autorotate.sh
In the history of this project you can see that it used to be just two components; one to rotate S.A.M. (Screen And Mice) and one to prevent that.  However due to past experiences with how well `iio-sensor-proxy` functioned in different distributions (some worked, some installed but didn't work and others wouldn't even install), made me decide to split the rotation bits from the automation bits.  Now you can assign keyboard shortcuts or key-combos and still rotate your "sam" in one go.

**DEPENDENCIES**

* grep (rotate-sam)
* awk (rotate-sam)
* xrandr (rotate-sam)
* xinput (rotate-sam/key-locker.sh)
* notify-send (key-locker.sh)
* yad (key-locker.sh)
* inotify-tools (autorotate-daemon/key-locker.sh)
* iio-sensor-proxy (autorotate-daemon)

**HOW IT WORKS**

Autorotate S.A.M. is broken up into 3 components, which all have their primairy goal.
* `rotate-sam` is the component that rotates the "Screen And Mice" which can be controlled manually.  It is broken out into its own component because that way this project can also be used on distributions without `iio-sensor-proxy` (or when it fails to work for some reason).
* `autorotate-daemon` is the component the uses the `monitor-sensor` (part of iio-sensor-proxy) to control rotate-sam without user interaction.
* `key-locker.sh` disables the keyboard to prevent accidental input when the laptop is rotated or flipped and the keys can be accidentally pressed.

**HOW TO SET IT UP**

Step 1] Add all components to your $PATH (specifically the `rotate-sam` component, because the `rotation-daemon` relies on that).  To find which directories are in your $PATH, you can issue `echo $PATH` in a terminal prompt.  For some some directories you need admin priviliges.

Step 2) Add `autorotate-daemon` to your Startup Programs.  However if you don't have `monitor-sensor` working (part of the `iio-sensor-proxy` packages.  You'll have to look up instructions on how to get it working on your distribution), you'll have to assign key-combos to all the directions you want to rotate your "sam".  At least you want `rotate-sam --normal` (or `-n`) assigned to a key-combo, the others are `--left` or `-l`, `--right` or `-r`, `--bottomup` or `-b`.  You can first try them out in the terminal to get a feel how they rotate on your device.

Step 3] Add key-locker.sh to a keyboard shortcut.  It'll add/remove a hidden file [`.rotation-lock`] to tmpfs which nullifies the autorotate script.  When rotation is enabled it also creates a systray icon that adds/removes another hidden file [`.keyboard-lock`] to (un)locks the keyboard.  This part is the least tested section and doesn't seem to work on Budgie (at least not the icon in systray to disable the keyboard part) and the screen keeps rotating because of `iio-sensor-proxy` on NixOS.
