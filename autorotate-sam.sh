#!/bin/bash
# https://github.com/gevasiliou/PythonTests/blob/master/autorotate.sh original source
# December 2020; tested and works on Medion Akoya E3213 (MD60986)
# This is a bash script to make screen autorotation possible based on device orientation
# This script *should* be added to startup applications for the user.

# Use multistage grep to find the display connector (VGA-1/eDP-1/etc)
SCREEN=$(xrandr | grep 'connected' | grep -v 'disconnected' | grep -oE '[a-zA-Z]+[\-]+[^ ]')

function Mice {
	for i in $(xinput list | grep -e "slave  pointer" | grep -Po 'id=\K[0-9]+') ; do
		xinput set-prop "$i" "Coordinate Transformation Matrix" $MATRIX #1 0 0 0 1 0 0 0 1
	done
}

# Launch monitor-sensor - store output in a variable on tmpfs (RAM) to be parsed by the rest script
killall monitor-sensor
monitor-sensor >> /dev/shm/sensor.log 2>&1 &

# Parse output of monitor-sensor to get the new orientation whenever the log file is updated. Possibles are: normal, bottom-up, right-up, left-up. Light data will be ignored
while inotifywait -e modify /dev/shm/sensor.log; do
	ORIENTATION=$(tail -n 1 /dev/shm/sensor.log | grep 'orientation' | grep -oE '[^ ]+$')  # Read the last line that was added to the file and get the orientation
	case "$ORIENTATION" in # Set the actions to be taken for each possible orientation
	normal)
		if [ ! -f /dev/shm/.rotation-lock ]; then
			xrandr --output $SCREEN --rotate normal
			MATRIX="1 0 0 0 1 0 0 0 1" ; Mice $?=$MATRIX;
		fi;;
	bottom-up)
		if [ ! -f /dev/shm/.rotation-lock ]; then
			xrandr --output $SCREEN --rotate inverted
			MATRIX="-1 0 1 0 -1 1 0 0 1" ; Mice $?=$MATRIX ;
		fi;;
	right-up)
		if [ ! -f /dev/shm/.rotation-lock ] ; then
			xrandr --output $SCREEN --rotate right
			MATRIX="0 1 0 -1 0 1 0 0 1" ; Mice $?=$MATRIX ;
		fi;;
	left-up)
		if [ ! -f /dev/shm/.rotation-lock ] ; then
			xrandr --output $SCREEN --rotate left
			MATRIX="0 -1 1 1 0 0 0 0 1" ; Mice $?=$MATRIX ;
		fi;;
	esac
done
